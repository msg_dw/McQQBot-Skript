package pro.sandiao.mcqqbot.skript;

import ch.njol.skript.Skript;
import ch.njol.skript.SkriptAddon;
import org.bukkit.plugin.java.JavaPlugin;

import java.io.IOException;

/**
 * Skript附属
 */
public class McQQBotSkriptPlugin extends JavaPlugin {

    @Override
    public void onEnable() {
        // 注册Skript扩展
        SkriptAddon skriptAddon = Skript.registerAddon(this);
        try {
            skriptAddon.loadClasses("pro.sandiao.mcqqbot.skript", "events", "conditions", "effects", "expressions", "classes");
            getLogger().info("Skript hook.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
