package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.MessageEvent;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Event Message")
@Events("Bot Message Event")
@Description({"事件中的消息", "存在于所有的消息相关事件", "通过 bot-message's content 获取消息内容", "The bot message in the event."})
@Examples({
        "bot-message"
})
@Since("1.0.0")
public class ExprEventMessage extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprEventMessage.class, Message.class, ExpressionType.SIMPLE, "[the] [event] (bot|qq)( |-)message");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof MessageEvent) {
                return new Message[]{((MessageEvent) event).getMessage()};
            }
        }
        return new Message[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "this event bot message";
    }
}
