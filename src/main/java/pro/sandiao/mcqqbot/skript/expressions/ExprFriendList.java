package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.contact.ContactList;
import net.mamoe.mirai.contact.Friend;
import org.bukkit.event.Event;

@Name("Friend List")
@Description({"获取机器人的所有好友", "Get all friends of the robot."})
@Examples({
        "friends list of \"Main\" bot"
})
@Since("1.0.0")
public class ExprFriendList extends SimpleExpression<Friend> {

    static {
        Skript.registerExpression(ExprFriendList.class, Friend.class, ExpressionType.SIMPLE, "[all] friend[s] list of %qqbot%");
    }

    private Expression<Bot> bot;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        bot = (Expression<Bot>) exprs[0];
        return true;
    }

    @Override
    protected Friend[] get(Event e) {
        ContactList<Friend> friend = bot.getSingle(e).getFriends();
        return friend.toArray(new Friend[friend.size()]);
    }

    @Override
    public boolean isSingle() {
        return false;
    }

    @Override
    public Class<? extends Friend> getReturnType() {
        return Friend.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "friend list of " + bot.toString(e, debug);
    }
}
