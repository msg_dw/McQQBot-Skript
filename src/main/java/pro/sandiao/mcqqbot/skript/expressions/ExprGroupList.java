package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.contact.ContactList;
import net.mamoe.mirai.contact.Group;
import org.bukkit.event.Event;

@Name("Group List")
@Description({"获取机器人的所有群", "Get all group chats of the robot."})
@Examples({
        "groups list of \"Main\" bot"
})
@Since("1.0.0")
public class ExprGroupList extends SimpleExpression<Group> {

    static {
        Skript.registerExpression(ExprGroupList.class, Group.class, ExpressionType.SIMPLE, "[all] group[s] list of %qqbot%");
    }

    private Expression<Bot> bot;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        bot = (Expression<Bot>) exprs[0];
        return true;
    }

    @Override
    protected Group[] get(Event e) {
        ContactList<Group> groups = bot.getSingle(e).getGroups();
        return groups.toArray(new Group[groups.size()]);
    }

    @Override
    public boolean isSingle() {
        return false;
    }

    @Override
    public Class<? extends Group> getReturnType() {
        return Group.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "group list of " + bot.toString(e, debug);
    }
}
