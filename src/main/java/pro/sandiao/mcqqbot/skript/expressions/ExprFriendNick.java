package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Friend;
import org.bukkit.event.Event;

@Name("Friend Nick")
@Description({"好友的QQ昵称(网名)", "Friend's name."})
@Examples({
        "bot receive friend message:",
        "\tqq-friend's nick"
})
@Since("1.0.0")
public class ExprFriendNick extends PropertyExpression<Friend, String> {

    static {
        Skript.registerExpression(ExprFriendNick.class, String.class, ExpressionType.PROPERTY, "([real]name|nick) of %qqfriend%", "%qqfriend%'[s] ([real]name|nick)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Friend>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Friend[] source) {
        return get(source, Friend::getNick);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "name of " + getExpr().toString(e, debug);
    }
}
