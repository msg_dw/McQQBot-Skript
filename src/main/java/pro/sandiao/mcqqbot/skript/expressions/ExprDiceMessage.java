package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.Dice;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("QQ Dice Message")
@Description({"QQ的骰子信息", "QQ Dice Emoji."})
@Examples({
        "# 自定义点数 这tm是作弊 Custom points.",
        "dice of 1",
        "dice of 6",
        "dice of random"
})
@RequiredPlugins("McQQBot-2.0.0BATE4")
@Since("1.0.1")
public class ExprDiceMessage extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprDiceMessage.class, Message.class, ExpressionType.SIMPLE, "[qq( |-)]dice of random", "[qq( |-)]dice of %int%");
    }

    private Expression<Integer> integer;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        if (matchedPattern == 2) {
            integer = (Expression<Integer>) exprs[0];
        }
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        return integer == null ? new Message[]{Dice.random()} : new Message[]{new Dice(integer.getSingle(e))};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "dice of " + integer.toString(e, debug);
    }
}
