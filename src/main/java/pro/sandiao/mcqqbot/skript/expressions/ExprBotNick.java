package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import org.bukkit.event.Event;

@Name("Bot Nick")
@Description({"机器人的QQ昵称(网名)", "Bot's name."})
@Examples({
        "name of bot from \"Main\"",
        "\"Main\" bot's nick"
})
@Since("1.0.0")
public class ExprBotNick extends PropertyExpression<Bot, String> {

    static {
        Skript.registerExpression(ExprBotNick.class, String.class, ExpressionType.PROPERTY, "([real]name|nick) of %qqbot%", "%qqbot%'[s] ([real]name|nick)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Bot>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Bot[] source) {
        return get(source, Bot::getNick);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "name of " + getExpr().toString(e, debug);
    }
}
