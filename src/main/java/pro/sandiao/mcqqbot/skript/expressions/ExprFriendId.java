package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Friend;
import org.bukkit.event.Event;

@Name("Friend ID")
@Description({"好友的QQ号", "Friend's id."})
@Examples({
        "bot receive friend message:",
        "\tqq-friend's id"
})
@Since("1.0.0")
public class ExprFriendId extends PropertyExpression<Friend, Long> {

    static {
        Skript.registerExpression(ExprFriendId.class, Long.class, ExpressionType.PROPERTY, "(id|code) of %qqfriend%", "%qqfriend%'[s] (id|code)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Friend>) exprs[0]);
        return true;
    }

    @Override
    protected Long[] get(Event e, Friend[] source) {
        return get(source, Friend::getId);
    }

    @Override
    public Class<? extends Long> getReturnType() {
        return Long.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "id of " + getExpr().toString(e, debug);
    }
}
