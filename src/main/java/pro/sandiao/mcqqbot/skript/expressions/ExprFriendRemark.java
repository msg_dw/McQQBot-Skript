package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Friend;
import org.bukkit.event.Event;

@Name("Friend Remark")
@Description({"好友的备注(仅自身可见)", "Remarks of the robot's friend."})
@Examples({
        "bot receive friend message:",
        "\tqq-friend's remark"
})
@Since("1.0.0")
public class ExprFriendRemark extends PropertyExpression<Friend, String> {

    static {
        Skript.registerExpression(ExprFriendRemark.class, String.class, ExpressionType.PROPERTY, "remark[s] of %qqfriend%", "%qqfriend%'[s] remark[s]");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Friend>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Friend[] source) {
        return get(source, Friend::getRemark);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "remark of " + getExpr().toString(e, debug);
    }
}