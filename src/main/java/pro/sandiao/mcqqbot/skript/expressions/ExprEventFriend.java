package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Friend;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.FriendEvent;
import org.bukkit.event.Event;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Event Friend")
@Events("Friend Event")
@Description({"事件中的好友", "存在于所有的好友相关事件", "The friend in the event."})
@Examples({
        "qq-friend"
})
@Since("1.0.0")
public class ExprEventFriend extends SimpleExpression<Friend> {

    static {
        Skript.registerExpression(ExprEventFriend.class, Friend.class, ExpressionType.SIMPLE, "[the] [event] qq( |-)friend");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected Friend[] get(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof FriendEvent) {
                return new Friend[]{((FriendEvent) event).getFriend()};
            }
        }
        return new Friend[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Friend> getReturnType() {
        return Friend.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "the event qq-friend";
    }
}