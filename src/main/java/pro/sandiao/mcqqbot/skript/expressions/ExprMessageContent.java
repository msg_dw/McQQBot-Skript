package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;

@Name("Message Content")
@Description({"消息内容", "Message's content, is string."})
@Examples({
        "bot receive group message:",
        "\tbot-message's content"
})
@Since("1.0.0")
public class ExprMessageContent extends PropertyExpression<Message, String> {

    static {
        Skript.registerExpression(ExprMessageContent.class, String.class, ExpressionType.PROPERTY, "content[s] of %qqmessage%", "%qqmessage%'[s] content[s]");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Message>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Message[] source) {
        return get(source, Message::contentToString);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "content of " + getExpr().toString(e, debug);
    }
}