package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.Face;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

import java.lang.reflect.Field;

@Name("QQ Emoje Message")
@Description({"QQ原生小表情", "QQ Original Emoji."})
@Examples({
        "# 爱心 = ❤, 心碎 = 💔",
        "emoji of \"爱心\"",
        "emoji of \"心碎\""
})
@Since("1.0.0")
public class ExprEmojiMessage extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprEmojiMessage.class, Message.class, ExpressionType.SIMPLE, "[qq( |-)]emoji of %string%");
    }

    private Expression<String> string;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        string = (Expression<String>) exprs[0];
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        try {
            Field field = Face.class.getField(string.getSingle(e));
            return new Message[]{new Face(field.getInt(null))};
        } catch (NoSuchFieldException | IllegalAccessException exception) {
            // 找不到这个表情
            Skript.error("找不到这个表情");
        }
        return new Message[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "emoji of " + string.toString(e, debug);
    }
}
