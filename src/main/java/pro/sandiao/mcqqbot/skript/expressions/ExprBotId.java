package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import org.bukkit.event.Event;

@Name("Bot ID")
@Description({"机器人的QQ号", "Bot's id."})
@Examples({
        "code of bot from \"Main\"",
        "\"Main\" bot's id"
})
@Since("1.0.0")
public class ExprBotId extends PropertyExpression<Bot, Long> {

    static {
        Skript.registerExpression(ExprBotId.class, Long.class, ExpressionType.PROPERTY, "(id|code) of %qqbot%", "%qqbot%'[s] (id|code)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Bot>) exprs[0]);
        return true;
    }

    @Override
    protected Long[] get(Event e, Bot[] source) {
        return get(source, Bot::getId);
    }

    @Override
    public Class<? extends Long> getReturnType() {
        return Long.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "id of " + getExpr().toString(e, debug);
    }
}
