package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.classes.Changer.ChangeMode;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import ch.njol.util.coll.CollectionUtils;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.NormalMember;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Group Member Special Title")
@Description({"QQ群成员头衔", "如果是匿名聊天 群头衔则为匿名", "Group member's title."})
@Examples({
        "bot receive group message:",
        "\tgroup-member's special-title"
})
@Since("1.0.0")
public class ExprMemberSpecialTitle extends PropertyExpression<Member, String> {

    static {
        Skript.registerExpression(ExprMemberSpecialTitle.class, String.class, ExpressionType.PROPERTY, "[special(|-)]title[s] of %qqgroupmember%", "%qqgroupmember%'[s] [special(|-)]title[s]");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        setExpr((Expression<? extends Member>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Member[] source) {
        return get(source, Member::getSpecialTitle);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "special-title of " + getExpr().toString(e, debug);
    }

    @Override
    public Class<?>[] acceptChange(ChangeMode mode) {
        if (mode == ChangeMode.SET || mode == ChangeMode.RESET) return CollectionUtils.array(String.class);
        return null;
    }

    @Override
    public void change(Event e, @Nullable Object[] delta, ChangeMode mode) {
        String title = mode == ChangeMode.SET ? delta[0].toString() : "";
        Member single = getExpr().getSingle(e);
        if (single instanceof NormalMember) ((NormalMember) single).setSpecialTitle(title);
    }
}
