package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.McQQBotPlugin;

import java.util.Collection;
import java.util.stream.Collectors;

@Name("Bot List")
@Description({"获取所有机器人", "Get all robot list."})
@Examples({
        "all bots list",
        "all online bots list"
})
@Since("1.0.0")
public class ExprBotList extends SimpleExpression<Bot> {

    static {
        Skript.registerExpression(ExprBotList.class, Bot.class, ExpressionType.SIMPLE, "all bot[s] [list]", "all online bot[s] [list]");
    }

    private boolean isOnline;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        isOnline = matchedPattern == 1;
        return true;
    }

    @Override
    protected Bot[] get(Event e) {
        Collection<Bot> bots = McQQBotPlugin.INSTANCE.getBotAccounts().values();
        if (isOnline) bots = bots.stream().filter(Bot::isOnline).collect(Collectors.toList());
        return bots.toArray(new Bot[bots.size()]);
    }

    @Override
    public boolean isSingle() {
        return false;
    }

    @Override
    public Class<? extends Bot> getReturnType() {
        return Bot.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        String online = isOnline ? " online " : " ";
        return "all" + online + "bot list";
    }
}
