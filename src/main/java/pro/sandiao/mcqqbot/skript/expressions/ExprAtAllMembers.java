package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.AtAll;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("At All Members")
@Description({"艾特全体成员", "Notify all members."})
@Examples({
        "@all",
        "at all",
        "at all members"
})
@Since("1.0.0")
public class ExprAtAllMembers extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprAtAllMembers.class, Message.class, ExpressionType.SIMPLE, "@all [member[s]]","at all [member[s]]");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        return new Message[]{AtAll.INSTANCE};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "at all members";
    }
}
