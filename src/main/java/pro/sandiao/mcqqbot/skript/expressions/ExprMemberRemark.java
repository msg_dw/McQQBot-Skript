package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Member;
import org.bukkit.event.Event;

@Name("Group Member Remark")
@Description({"QQ群成员的备注", "Group member's remark."})
@Examples({
        "bot receive group message:",
        "\tgroup-member's remark"
})
@Since("1.0.0")
public class ExprMemberRemark extends PropertyExpression<Member, String> {

    static {
        Skript.registerExpression(ExprMemberRemark.class, String.class, ExpressionType.PROPERTY, "remark[s] of %qqgroupmember%", "%qqgroupmember%'[s] remark[s]");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Member>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Member[] source) {
        return get(source, Member::getRemark);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "remark of " + getExpr().toString(e, debug);
    }
}
