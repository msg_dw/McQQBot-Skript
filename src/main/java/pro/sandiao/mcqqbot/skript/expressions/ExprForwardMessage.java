package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.MessageEvent;
import net.mamoe.mirai.message.data.ForwardMessageBuilder;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("QQ Forward Message")
@Description({"QQ转发消息", "Forward message."})
@Examples({
        "on bot receive group message:",
        "\t# 转发至另一个群 Forward the message to another group.",
        "\tsend forward message to group 10086 of bot",
})
@Since("1.0.0")
public class ExprForwardMessage extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprForwardMessage.class, Message.class, ExpressionType.SIMPLE, "forward message");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof MessageEvent) {
                MessageEvent messageEvent = (MessageEvent) event;
                return new Message[]{new ForwardMessageBuilder((messageEvent).getSubject()).add(messageEvent).build()};
            }
        }
        return new Message[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "forward message";
    }
}
