package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.MessageChain;
import net.mamoe.mirai.message.data.MessageChainBuilder;
import org.bukkit.event.Event;

@Name("Message To JSON")
@Description({"JSON的序列化信息", "Json serialize message."})
@Examples({
        "bot receive group message:",
        "\tbroadcast json of event-message"
})
@Since("1.0.3")
public class ExprMessageToJson extends SimpleExpression<String> {

    static {
        Skript.registerExpression(ExprMessageToJson.class, String.class, ExpressionType.SIMPLE, "json (from|of) %qqmessage%");
    }

    private Expression<Message> message;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        message = (Expression<Message>) exprs[0];
        return true;
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    protected String[] get(Event e) {
        Message o = message.getSingle(e);
        MessageChain messageChain;
        if (o instanceof MessageChain) {
            messageChain = (MessageChain) o;
        } else {
            MessageChainBuilder builder = new MessageChainBuilder();
            messageChain = builder.append(o).build();
        }
        return new String[] {MessageChain.serializeToJsonString(messageChain)};
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "bot message of json " + message.toString(e, debug);
    }
}
