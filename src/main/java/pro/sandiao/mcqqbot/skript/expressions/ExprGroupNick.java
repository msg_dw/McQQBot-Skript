package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.classes.Changer.ChangeMode;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import ch.njol.util.coll.CollectionUtils;
import net.mamoe.mirai.contact.Group;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Group Nick")
@Description({"QQ群昵称(群名)", "如果机器人是管理员 那么它可以修改群名", "Group's name, Can change."})
@Examples({
        "bot receive group message:",
        "\tqq-group's nick"
})
@Since("1.0.0")
public class ExprGroupNick extends PropertyExpression<Group, String> {

    static {
        Skript.registerExpression(ExprGroupNick.class, String.class, ExpressionType.PROPERTY, "([real]name|nick) of %qqgroup%", "%qqgroup%'[s] ([real]name|nick)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Group>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Group[] source) {
        return get(source, Group::getName);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "name of " + getExpr().toString(e, debug);
    }

    @Override
    public Class<?>[] acceptChange(ChangeMode mode) {
        if (mode == ChangeMode.SET) return CollectionUtils.array(String.class);
        return null;
    }

    @Override
    public void change(Event e, @Nullable Object[] delta, ChangeMode mode) {
        getExpr().getSingle(e).setName(delta[0].toString());
    }
}
