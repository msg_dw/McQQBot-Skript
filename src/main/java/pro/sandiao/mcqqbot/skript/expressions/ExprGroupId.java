package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Group;
import org.bukkit.event.Event;

@Name("Group ID")
@Description({"QQ群号", "Group's id."})
@Examples({
        "bot receive group message:",
        "\tqq-group's id"
})
@Since("1.0.0")
public class ExprGroupId extends PropertyExpression<Group, Long> {

    static {
        Skript.registerExpression(ExprGroupId.class, Long.class, ExpressionType.PROPERTY, "(id|code) of %qqgroup%", "%qqgroup%'[s] (id|code)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Group>) exprs[0]);
        return true;
    }

    @Override
    protected Long[] get(Event e, Group[] source) {
        return get(source, Group::getId);
    }

    @Override
    public Class<? extends Long> getReturnType() {
        return Long.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "id of " + getExpr().toString(e, debug);
    }
}
