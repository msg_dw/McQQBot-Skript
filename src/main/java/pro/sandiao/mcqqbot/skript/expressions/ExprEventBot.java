package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import org.bukkit.event.Event;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Event Bot")
@Events("Bot Event")
@Description({"事件中的机器人", "存在于所有的机器人相关事件", "The robot in the event."})
@Examples({
        "bot",
        "event-bot"
})
@Since("1.0.0")
public class ExprEventBot extends SimpleExpression<Bot> {

    static {
        Skript.registerExpression(ExprEventBot.class, Bot.class, ExpressionType.PROPERTY, "[the] [event( |-)]bot");
    }

    @Override
    public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, ParseResult parseResult) {
        return true;
    }

    @Override
    public Class<? extends Bot> getReturnType() {
        return Bot.class;
    }

    @Override
    protected Bot[] get(Event event) {
        return event instanceof BukkitMcBotEvent ? new Bot[]{((BukkitMcBotEvent) event).getBot()} : new Bot[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public String toString(Event event, boolean b) {
        return "the event bot";
    }
}
