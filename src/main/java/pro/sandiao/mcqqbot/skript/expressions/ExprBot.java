package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import org.bukkit.event.Event;
import pro.sandiao.mcqqbot.McQQBotPlugin;

@Name("Bot")
@Description({"获取到机器人", "Get a Robot."})
@Examples({
        "bot from \"Main\"",
        "\"Main\" bot"
})
@Since("1.0.0")
public class ExprBot extends SimpleExpression<Bot> {

    static {
        Skript.registerExpression(ExprBot.class, Bot.class, ExpressionType.PROPERTY, "bot (from|of) %string%", "%string% bot");
    }

    private Expression<String> botName;

    @Override
    public boolean init(Expression<?>[] expressions, int i, Kleenean kleenean, ParseResult parseResult) {
        botName = (Expression<String>) expressions[0];
        return true;
    }

    @Override
    public Class<? extends Bot> getReturnType() {
        return Bot.class;
    }

    @Override
    protected Bot[] get(Event event) {
        Bot bot = McQQBotPlugin.INSTANCE.getBotAccounts().get(botName.getSingle(event));
        return (bot != null) ? new Bot[]{bot} : new Bot[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public String toString(Event event, boolean b) {
        return "bot from " + botName.toString(event, b);
    }
}
