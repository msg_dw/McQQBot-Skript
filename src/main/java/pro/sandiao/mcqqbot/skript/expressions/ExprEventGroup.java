package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.GroupEvent;
import org.bukkit.event.Event;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Event Group")
@Events("Group Event")
@Description({"事件中的群", "存在于所有的群相关事件", "The group in the event."})
@Examples({
        "qq-group"
})
@Since("1.0.0")
public class ExprEventGroup extends SimpleExpression<Group> {

    static {
        Skript.registerExpression(ExprEventGroup.class, Group.class, ExpressionType.SIMPLE, "[the] [event] qq( |-)group");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected Group[] get(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof GroupEvent) {
                return new Group[]{((GroupEvent) event).getGroup()};
            }
        }
        return new Group[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Group> getReturnType() {
        return Group.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "the event qq-group";
    }
}
