package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.MemberKt;
import net.mamoe.mirai.message.data.At;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("At Member")
@Description({"艾特一个成员", "Notify a member."})
@Examples({
        "@10086",
        "at member 10086"
})
@Since("1.0.0")
public class ExprAtMember extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprAtMember.class, Message.class, ExpressionType.SIMPLE, "@[member ]%long%", "at [group] member %long%");
    }

    private Expression<Long> code;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        code = (Expression<Long>) exprs[0];
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        return new Message[]{new At(code.getSingle(e))};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "at group member " + code.toString(e, debug);
    }
}
