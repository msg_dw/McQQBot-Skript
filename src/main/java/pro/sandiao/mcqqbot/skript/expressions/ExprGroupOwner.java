package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.contact.Member;
import org.bukkit.event.Event;

@Name("Group Owner")
@Description({"QQ群所有者(群主)", "返回的是一个成员 群主也是成员之一", "Group's owner, it is a member."})
@Examples({
        "bot receive group message:",
        "\tqq-group's owner"
})
@Since("1.0.0")
public class ExprGroupOwner extends PropertyExpression<Group, Member> {

    static {
        Skript.registerExpression(ExprGroupOwner.class, Member.class, ExpressionType.PROPERTY, "owner of %qqgroup%", "%qqgroup%'[s] owner");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        setExpr((Expression<? extends Group>) exprs[0]);
        return true;
    }

    @Override
    protected Member[] get(Event e, Group[] source) {
        return get(source, Group::getOwner);
    }

    @Override
    public Class<? extends Member> getReturnType() {
        return Member.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "owner of " + getExpr().toString(e, debug);
    }
}
