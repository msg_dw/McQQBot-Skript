package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.MessageChainBuilder;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Message Plus Message")
@Description({"拼接消息内容", "Splicing messages."})
@Examples({
        "@all + \"号外号外!\"",
        "\"你好啊!\" + @10086",
        "# 接收到群消息是 回复 @ta + ta发的内容",
        "bot receive group message:",
        "\treply @group-member's id + \" \" + qq-message"
})
public class ExprMessagePlusMessage extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprMessagePlusMessage.class, Message.class, ExpressionType.SIMPLE, "%qqmessage% (+|plus|append) %qqmessage%");
    }

    private Expression<Message> before;
    private Expression<Message> after;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        before = (Expression<Message>) exprs[0];
        after = (Expression<Message>) exprs[1];
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        MessageChainBuilder messages = new MessageChainBuilder();
        Message beforeSingle = before.getSingle(e);
        Message afterSingle = after.getSingle(e);
        return new Message[]{messages.append(beforeSingle).append(afterSingle).build()};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return before.toString(e, debug) + " append " + after.toString(e, debug);
    }
}
