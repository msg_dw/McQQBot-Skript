package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.MessageChain;
import org.bukkit.event.Event;

@Name("JSON To Message")
@Description({"JSON的反序列化信息", "Json deserialize message."})
@Examples({
        "bot receive group message:",
        "\t# 惊喜的表情",
        "\treply bot-message of json \"[{\"\"type\"\":\"\"Face\"\",\"\"id\"\":180}]\""
})
@Since("1.0.3")
public class ExprJsonToMessage extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprJsonToMessage.class, Message.class, ExpressionType.SIMPLE, "(bot|qq)( |-)message (from|of) json %string%");
    }

    private Expression<String> message;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        message = (Expression<String>) exprs[0];
        return true;
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    protected Message[] get(Event e) {
        return new Message[] {MessageChain.deserializeFromJsonString(message.getSingle(e))};
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "bot message of json " + message.toString(e, debug);
    }
}
