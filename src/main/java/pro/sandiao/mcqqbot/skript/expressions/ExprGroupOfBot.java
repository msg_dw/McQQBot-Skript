package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.contact.Group;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Group Of Bot")
@Description({"获取机器人的群", "Get group chat of robots by id"})
@Examples({
        "group 10086 of \"Main\" bot"
})
@Since("1.0.0")
public class ExprGroupOfBot extends SimpleExpression<Group> {

    static {
        Skript.registerExpression(ExprGroupOfBot.class, Group.class, ExpressionType.SIMPLE, "group [of] %long% (of|by) %qqbot%");
    }

    private Expression<Long> code;
    private Expression<Bot> bot;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        code = (Expression<Long>) exprs[0];
        bot = (Expression<Bot>) exprs[1];
        return true;
    }

    @Override
    protected Group[] get(Event e) {
        Group group = bot.getSingle(e).getGroup(code.getSingle(e));
        if (group != null) return new Group[]{group};
        return new Group[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Group> getReturnType() {
        return Group.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "group " + code.toString(e, debug) + " by " + bot.getSingle(e);
    }
}
