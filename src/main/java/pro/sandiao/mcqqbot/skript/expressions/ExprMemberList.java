package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.ContactList;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.contact.Member;
import org.bukkit.event.Event;

@Name("Group Member List")
@Description({"获取群的所有成员", "Get all members of group"})
@Examples({
        "members list of group 10086 of \"Main\" bot"
})
@Since("1.0.0")
public class ExprMemberList extends SimpleExpression<Member> {

    static {
        Skript.registerExpression(ExprMemberList.class, Member.class, ExpressionType.SIMPLE, "[all] member[s] list of %qqgroup%");
    }

    private Expression<Group> group;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        group = (Expression<Group>) exprs[0];
        return true;
    }

    @Override
    protected Member[] get(Event e) {
        ContactList<? extends Member> members = group.getSingle(e).getMembers();
        return members.toArray(new Member[members.size()]);
    }

    @Override
    public boolean isSingle() {
        return false;
    }

    @Override
    public Class<? extends Member> getReturnType() {
        return Member.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "members list of " + group.toString(e, debug);
    }
}
