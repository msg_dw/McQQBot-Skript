package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Friend;
import net.mamoe.mirai.contact.Member;
import org.bukkit.event.Event;

@Name("Group Member ID")
@Description({"QQ群成员QQ号", "Group member's id"})
@Examples({
        "bot receive group message:",
        "\tgroup-member's id"
})
@Since("1.0.0")
public class ExprMemberId extends PropertyExpression<Member, Long> {

    static {
        Skript.registerExpression(ExprMemberId.class, Long.class, ExpressionType.PROPERTY, "(id|code) of %qqgroupmember%", "%qqgroupmember%'[s] (id|code)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Member>) exprs[0]);
        return true;
    }

    @Override
    protected Long[] get(Event e, Member[] source) {
        return get(source, Member::getId);
    }

    @Override
    public Class<? extends Long> getReturnType() {
        return Long.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "id of " + getExpr().toString(e, debug);
    }
}
