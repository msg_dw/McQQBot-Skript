package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.PlainText;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Text Message")
@Description({"文本消息", "一般不会使用 因为可以事件回复 reply \"你好!\"", "Get text message."})
@Examples({
        "bot receive group message:",
        "\t# 你好 = hello",
        "\treply bot-message of \"你好!\""
})
@Since("1.0.0")
public class ExprTextMessage extends SimpleExpression<Message> {

    static {
        Skript.registerExpression(ExprTextMessage.class, Message.class, ExpressionType.SIMPLE, "(bot|qq)( |-)message (from|of) [text] %string%");
    }

    private Expression<String> message;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        message = (Expression<String>) exprs[0];
        return true;
    }

    @Override
    protected Message[] get(Event e) {
        return new Message[]{new PlainText(message.getSingle(e))};
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Message> getReturnType() {
        return Message.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "bot message from " + message.toString(e, debug);
    }
}