package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.classes.Changer.ChangeMode;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import ch.njol.util.coll.CollectionUtils;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.NormalMember;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Group Member Name Card")
@Description({"QQ群成员的名片", "如果机器人是管理员 那么它可以修改其它群成员的名片", "如果它是群主 则可以修改所有人的名片", "Group member's group display name, Can change."})
@Examples({
        "bot receive group message:",
        "\tgroup-member's namecard"
})
@Since("1.0.0")
public class ExprMemberNameCard extends PropertyExpression<Member, String> {

    static {
        Skript.registerExpression(ExprMemberNameCard.class, String.class, ExpressionType.PROPERTY, "[member] (name|nick)[(|-)]card of %qqgroupmember%", "%qqgroupmember%'[s] [member] (name|nick)[(|-)]card");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Member>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Member[] source) {
        return get(source, Member::getNameCard);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "namecard of " + getExpr().toString(e, debug);
    }

    @Override
    public Class<?>[] acceptChange(ChangeMode mode) {
        if (mode == ChangeMode.SET) return CollectionUtils.array(String.class);
        return null;
    }

    @Override
    public void change(Event e, @Nullable Object[] delta, ChangeMode mode) {
        Member single = getExpr().getSingle(e);
        if (single instanceof NormalMember) ((NormalMember) single).setNameCard(delta[0].toString());
    }
}
