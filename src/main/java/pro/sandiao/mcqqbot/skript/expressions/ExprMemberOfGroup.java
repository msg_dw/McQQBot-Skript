package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.NormalMember;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Member Of Group")
@Description({"获取群的成员", "Get the members of the group."})
@Examples({
        "# 碰巧QQ和群号都是10086 One billionth, The group id is the same as the member id.",
        "member 10086 of group 10086 of \"Main\" bot"
})
@Since("1.0.0")
public class ExprMemberOfGroup extends SimpleExpression<Member> {

    static {
        Skript.registerExpression(ExprMemberOfGroup.class, Member.class, ExpressionType.SIMPLE, "[group] member [of] %long% (of|by) %qqgroup%");
    }

    private Expression<Long> code;
    private Expression<Group> group;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        code = (Expression<Long>) exprs[0];
        group = (Expression<Group>) exprs[1];
        return true;
    }

    @Override
    protected Member[] get(Event e) {
        NormalMember member = group.getSingle(e).get(code.getSingle(e));
        if (member != null) return new Member[]{member};
        return new Member[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Member> getReturnType() {
        return Member.class;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "member " + code.toString(e, debug) + " by " + group.getSingle(e);
    }
}
