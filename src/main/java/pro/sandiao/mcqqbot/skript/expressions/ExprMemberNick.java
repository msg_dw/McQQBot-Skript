package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.expressions.base.PropertyExpression;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Member;
import org.bukkit.event.Event;

@Name("Group Member Nick")
@Description({"QQ群成员的昵称(网名)", "Group member's name"})
@Examples({
        "bot receive group message:",
        "\tgroup-member's nick"
})
@Since("1.0.0")
public class ExprMemberNick extends PropertyExpression<Member, String> {

    static {
        Skript.registerExpression(ExprMemberNick.class, String.class, ExpressionType.PROPERTY, "([real]name|nick) of %qqgroupmember%", "%qqgroupmember%'[s] ([real]name|nick)");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, ParseResult parseResult) {
        setExpr((Expression<? extends Member>) exprs[0]);
        return true;
    }

    @Override
    protected String[] get(Event e, Member[] source) {
        return get(source, Member::getNick);
    }

    @Override
    public Class<? extends String> getReturnType() {
        return String.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "name of " + getExpr().toString(e, debug);
    }
}
