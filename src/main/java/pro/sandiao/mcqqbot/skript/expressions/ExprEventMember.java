package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Friend;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.event.events.*;
import org.bukkit.event.Event;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Event Member")
@Events("Group Member Event")
@Description({"事件中的群成员", "存在于所有的群成员相关事件", "The group member in the event."})
@Examples({
        "group-member"
})
@Since("1.0.0")
public class ExprEventMember extends SimpleExpression<Member> {

    static {
        Skript.registerExpression(ExprEventMember.class, Member.class, ExpressionType.SIMPLE, "[the] [event] group( |-)member");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected Member[] get(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof GroupMemberEvent) {
                return new Member[]{((GroupMemberEvent) event).getMember()};
            }
            if (event instanceof GroupMessageEvent) {
                return new Member[]{((GroupMessageEvent) event).getSender()};
            }
        }
        return new Member[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Member> getReturnType() {
        return Member.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "the event group member";
    }
}
