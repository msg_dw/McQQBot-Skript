package pro.sandiao.mcqqbot.skript.expressions;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.ExpressionType;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.lang.util.SimpleExpression;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.contact.Friend;
import org.bukkit.event.Event;

@Name("Friend Of Bot")
@Description({"获取机器人的好友", "Get a friend of a robot by id."})
@Examples({
        "friend 10086 of \"Main\" bot"
})
@Since("1.0.0")
public class ExprFriendOfBot extends SimpleExpression<Friend> {

    static {
        Skript.registerExpression(ExprFriendOfBot.class, Friend.class, ExpressionType.SIMPLE, "friend [of] %long% (of|by) %qqbot%");
    }

    private Expression<Long> code;
    private Expression<Bot> bot;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        code = (Expression<Long>) exprs[0];
        bot = (Expression<Bot>) exprs[1];
        return true;
    }

    @Override
    protected Friend[] get(Event e) {
        Friend friend = bot.getSingle(e).getFriend(code.getSingle(e));
        if (friend != null) return new Friend[]{friend};
        return new Friend[0];
    }

    @Override
    public boolean isSingle() {
        return true;
    }

    @Override
    public Class<? extends Friend> getReturnType() {
        return Friend.class;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "friend " + code.toString(e, debug) + " by " + bot.getSingle(e);
    }
}
