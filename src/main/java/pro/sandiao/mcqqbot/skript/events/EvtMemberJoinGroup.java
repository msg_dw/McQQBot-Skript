package pro.sandiao.mcqqbot.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SkriptEvent;
import ch.njol.skript.lang.SkriptParser;
import net.mamoe.mirai.event.events.MemberJoinEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.NotNull;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Member Join Group")
@Description({"成员加入群事件 加入之后", "New members join group chat. in After."})
@Examples({
        "member join group:",
        "\t# 欢迎欢迎 热烈欢迎! = welcome welcome hot welcome!",
        "\tsend \"欢迎欢迎 热烈欢迎!\" to qq-group"
})
@Since("1.0.0")
public class EvtMemberJoinGroup extends SkriptEvent {

    static {
        Skript.registerEvent("Member Join Group", EvtMemberJoinGroup.class, BukkitMcBotEvent.class, "member join group");
    }

    @Override
    public boolean init(Literal<?> @NotNull [] args, int matchedPattern, @NotNull SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    public boolean check(Event e) {
        return e instanceof BukkitMcBotEvent && ((BukkitMcBotEvent) e).getEvent() instanceof MemberJoinEvent;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "member join group";
    }
}
