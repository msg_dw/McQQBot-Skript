package pro.sandiao.mcqqbot.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SkriptEvent;
import ch.njol.skript.lang.SkriptParser;
import net.mamoe.mirai.event.events.MemberLeaveEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.NotNull;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Member Leave Group")
@Description({"成员离开群事件 离开之后", "Old members leave the group chat. in After."})
@Examples({
        "member leave group:",
        "\t# 一路走好! = a road go good!",
        "\tsend \"%group-member's name% 一路走好!\" to qq-group"
})
@Since("1.0.0")
public class EvtMemberLeaveGroup extends SkriptEvent {

    static {
        Skript.registerEvent("Member Leave Group", EvtMemberLeaveGroup.class, BukkitMcBotEvent.class, "member leave group");
    }

    @Override
    public boolean init(Literal<?> @NotNull [] args, int matchedPattern, @NotNull SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    public boolean check(Event e) {
        return e instanceof BukkitMcBotEvent && ((BukkitMcBotEvent) e).getEvent() instanceof MemberLeaveEvent;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "member leave group";
    }
}
