package pro.sandiao.mcqqbot.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SkriptEvent;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import net.mamoe.mirai.event.events.MessageEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.NotNull;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Bot Message Event")
@Events({"Bot Friend Message Event", "Bot Group Message Event", "Bot Group Temp Message Event"})
@Description({"机器人接收到消息事件","所有种类的消息", "Received a message."})
@Examples({
        "bot receive message:"
})
@Since("1.0.0")
public class EvtBotMessage extends SkriptEvent {

    static {
        Skript.registerEvent("Bot Message Event", EvtBotMessage.class, BukkitMcBotEvent.class, "bot [receiv(e|ing)] message").description("机器人接收到消息的事件");
    }

    @Override
    public boolean init(Literal<?> @NotNull [] args, int matchedPattern, @NotNull ParseResult parseResult) {
        return true;
    }

    @Override
    public boolean check(Event e) {
        return e instanceof BukkitMcBotEvent && ((BukkitMcBotEvent) e).getEvent() instanceof MessageEvent;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "bot receive message";
    }
}
