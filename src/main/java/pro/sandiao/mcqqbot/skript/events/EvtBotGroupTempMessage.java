package pro.sandiao.mcqqbot.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SkriptEvent;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import net.mamoe.mirai.event.events.GroupTempMessageEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.NotNull;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Bot Group Temp Message Event")
@Description({"机器人接收到群临时消息(临时会话)事件", "Received a temporary chat from group members."})
@Examples({
        "bot receive group temp message:"
})
@Since("1.0.0")
public class EvtBotGroupTempMessage extends SkriptEvent {

    static {
        Skript.registerEvent("Bot Group Temp Message Event", EvtBotGroupTempMessage.class, BukkitMcBotEvent.class, "bot [receiv(e|ing)] [group] temp message").description("机器人接收到群消息的事件");
    }

    @Override
    public boolean init(Literal<?> @NotNull [] args, int matchedPattern, @NotNull ParseResult parseResult) {
        return true;
    }

    @Override
    public boolean check(Event e) {
        return e instanceof BukkitMcBotEvent && ((BukkitMcBotEvent) e).getEvent() instanceof GroupTempMessageEvent;
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "bot receive group temp message";
    }
}
