package pro.sandiao.mcqqbot.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SkriptEvent;
import ch.njol.skript.lang.SkriptParser;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.BotInvitedJoinGroupRequestEvent;
import net.mamoe.mirai.event.events.MemberJoinRequestEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Member Request Join Group")
@Description({"成员请求加入群事件", "你可以接受或拒绝加群", "New member requests to join group chat, is in Accept or reject here"})
@Examples({
        "member request join group:",
        "\t# 朕批准了 I approved",
        "\taccept join group",
        "member be invited join group:",
        "\t# 本群只接受邀请制 = This group only accepts invitation.",
        "\treject join group due to \"本群只接受邀请制\""
})
@Since("1.0.0")
public class EvtMemberRequestJoinGroup extends SkriptEvent {

    static {
        Skript.registerEvent("Member Request Join Group", EvtMemberRequestJoinGroup.class, BukkitMcBotEvent.class, "member (request|1¦be invite[d]) join [group]");
    }

    private boolean isInvited;

    @Override
    public boolean init(Literal<?>[] args, int matchedPattern, SkriptParser.ParseResult parseResult) {
        isInvited = parseResult.mark == 1;
        return true;
    }

    @Override
    public boolean check(Event e) {
        if (!(e instanceof BukkitMcBotEvent)) return false;
        BotEvent event = ((BukkitMcBotEvent) e).getEvent();
        if (!(event instanceof MemberJoinRequestEvent)) return false;
        return !isInvited || ((MemberJoinRequestEvent) event).getInvitorId() != null;
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "member request join group";
    }
}
