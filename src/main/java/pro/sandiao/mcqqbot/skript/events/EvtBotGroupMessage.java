package pro.sandiao.mcqqbot.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SkriptEvent;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.GroupMessageEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.NotNull;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Bot Group Message Event")
@Description({"机器人接收到群消息事件", "Group chat message received"})
@Examples({
        "bot receive group message:",
        "# 机器人接收到10086的群消息 Receive a group chat message from the account with id 10086.",
        "bot receive group message by 10086:"
})
@Since("1.0.0")
public class EvtBotGroupMessage extends SkriptEvent {

    static {
        Skript.registerEvent("Bot Group Message Event", EvtBotGroupMessage.class, BukkitMcBotEvent.class, "bot [receiv(e|ing)] group message [(from|by) %-long%]");
    }

    private Literal<Long> code;

    @Override
    public boolean init(Literal<?> @NotNull [] args, int matchedPattern, @NotNull ParseResult parseResult) {
        code = (Literal<Long>) args[0];
        return true;
    }

    @Override
    public boolean check(Event e) {
        if (!(e instanceof BukkitMcBotEvent)) return false;
        BotEvent event = ((BukkitMcBotEvent) e).getEvent();
        if (!(event instanceof GroupMessageEvent)) return false;
        return code == null || ((GroupMessageEvent) event).getGroup().getId() == code.getSingle(e);
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "bot receive group message" + (code == null ? "" : code.toString(e, debug));
    }
}
