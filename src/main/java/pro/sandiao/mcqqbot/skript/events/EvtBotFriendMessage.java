package pro.sandiao.mcqqbot.skript.events;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Literal;
import ch.njol.skript.lang.SkriptEvent;
import ch.njol.skript.lang.SkriptParser.ParseResult;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.FriendMessageEvent;
import net.mamoe.mirai.event.events.GroupMessageEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.NotNull;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Bot Friend Message Event")
@Description({"机器人接收到好友消息事件", "Private chat message received"})
@Examples({
        "bot receive friend message:",
        "# 机器人接收到10086的好友消息 Receive a private chat message from the account with id 10086.",
        "bot receive friend message by 10086:"
})
@Since("1.0.0")
public class EvtBotFriendMessage extends SkriptEvent {

    static {
        Skript.registerEvent("Bot Friend Message Event", EvtBotFriendMessage.class, BukkitMcBotEvent.class, "bot [receiv(e|ing)] friend message [(from|by) %-long%]");
    }

    private Literal<Long> code;

    @Override
    public boolean init(Literal<?> @NotNull [] args, int matchedPattern, @NotNull ParseResult parseResult) {
        code = (Literal<Long>) args[0];
        return true;
    }

    @Override
    public boolean check(Event e) {
        if (!(e instanceof BukkitMcBotEvent)) return false;
        BotEvent event = ((BukkitMcBotEvent) e).getEvent();
        if (!(event instanceof FriendMessageEvent)) return false;
        return code == null || ((FriendMessageEvent) event).getFriend().getId() == code.getSingle(e);
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "bot receive friend message" + (code == null ? "" : code.toString(e, debug));
    }
}
