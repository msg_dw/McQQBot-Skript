package pro.sandiao.mcqqbot.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.*;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.MemberPermission;

@Name("Member Is Owner")
@Description({"判断群成员是否是群主", "Determine whether the group member is the group owner."})
@Examples({
        "bot receive group message:",
        "\tgroup-member is owner",
        "\treply @group-member's id + \"群主好\""
})
@Since("1.0.2")
public class CondMemberIsOwner extends PropertyCondition<Member> {

    static {
        register(CondMemberIsOwner.class, "owner", "qqgroupmember");
    }

    @Override
    public boolean check(Member member) {
        return member.getPermission() == MemberPermission.OWNER;
    }

    @Override
    protected String getPropertyName() {
        return "owner";
    }
}
