package pro.sandiao.mcqqbot.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.*;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.NormalMember;

@Name("Member Is Friend")
@Description({"判断群成员是否是好友", "Determine whether group members are friends."})
@Examples({
        "bot receive group message:",
        "\tgroup-member is friend",
        "\treply @group-member's id + \"+1\""
})
@RequiredPlugins("McQQBot-2.0.1")
@Since("1.0.2")
public class CondMemberIsMuted extends PropertyCondition<Member> {

    static {
        register(CondMemberIsMuted.class, "muted", "qqgroupmember");
    }

    @Override
    public boolean check(Member member) {
        return member instanceof NormalMember && ((NormalMember) member).isMuted();
    }

    @Override
    protected String getPropertyName() {
        return "muted";
    }
}
