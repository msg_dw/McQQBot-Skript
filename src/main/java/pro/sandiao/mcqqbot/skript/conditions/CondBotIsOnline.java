package pro.sandiao.mcqqbot.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import net.mamoe.mirai.Bot;

@Name("Bot Is Online")
@Description({"判断机器人是否在线(可以发送信息)", "Online status of the robot."})
@Examples({
        "bot receive group message:",
        "# 这是个废话 能接收到消息肯定就在线了.",
        "\tbot is online"
})
@Since("1.0.1")
public class CondBotIsOnline extends PropertyCondition<Bot> {

    static {
        register(CondBotIsOnline.class, "online", "qqbot");
    }

    @Override
    public boolean check(Bot bot) {
        return bot.isOnline();
    }

    @Override
    protected String getPropertyName() {
        return "online";
    }
}
