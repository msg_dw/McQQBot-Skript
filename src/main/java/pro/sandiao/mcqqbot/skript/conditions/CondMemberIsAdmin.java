package pro.sandiao.mcqqbot.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.MemberPermission;
import net.mamoe.mirai.contact.NormalMember;

@Name("Member Is Admin")
@Description({"判断群成员是否是群管理", "Determine whether the group member is the group administrator."})
@Examples({
        "bot receive group message:",
        "\tgroup-member is administrator",
        "\treply @group-member's id + \"管理好\""
})
@Since("1.0.2")
public class CondMemberIsAdmin extends PropertyCondition<Member> {

    static {
        register(CondMemberIsAdmin.class, "administrator", "qqgroupmember");
    }

    @Override
    public boolean check(Member member) {
        return member.getPermission() == MemberPermission.ADMINISTRATOR;
    }

    @Override
    protected String getPropertyName() {
        return "administrator";
    }
}
