package pro.sandiao.mcqqbot.skript.conditions;

import ch.njol.skript.conditions.base.PropertyCondition;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import net.mamoe.mirai.contact.Member;

@Name("Member Is Friend")
@Description({"判断群成员是否是好友", "Determine whether group members are friends."})
@Examples({
        "bot receive group message:",
        "\tgroup-member is friend",
        "\treply @group-member's id + \"+1\""
})
@Since("1.0.0")
public class CondMemberIsFriend extends PropertyCondition<Member> {

    static {
        register(CondMemberIsFriend.class, "friend", "qqgroupmember");
    }

    @Override
    public boolean check(Member member) {
        return member.getBot().getFriends().contains(member.getId());
    }

    @Override
    protected String getPropertyName() {
        return "friend";
    }
}
