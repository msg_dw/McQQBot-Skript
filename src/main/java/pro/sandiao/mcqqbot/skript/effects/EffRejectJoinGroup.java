package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.MemberJoinRequestEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Reject Join Group")
@Events("Member Request Join Group")
@Description({"在请求加群事件中拒绝加群", "Reject members from joining group chat, is in Member Request Join Group Event."})
@Examples({
        "member request join group:",
        "\t # 你长得不好看 = you are shit",
        "\treject join group by reason of \"你长得不好看\"",
        "reject join group"
})
@Since("1.0.0")
public class EffRejectJoinGroup extends Effect {

    static {
        Skript.registerEffect(EffRejectJoinGroup.class, "(deny|reject) join group [(by reason of|because [of]|due to) %-string%]");
    }

    private Expression<String> rejectMessage;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        rejectMessage = (Expression<String>) exprs[0];
        return true;
    }

    @Override
    protected void execute(Event e) {
        String reason = rejectMessage != null ? rejectMessage.getSingle(e) : "";
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof MemberJoinRequestEvent) {
                ((MemberJoinRequestEvent) event).reject(false, reason);
            }
        }
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "reject join group" + (rejectMessage != null ? " due to " + rejectMessage.toString(e, debug) : "");
    }
}
