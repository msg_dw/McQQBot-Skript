package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.NormalMember;
import org.bukkit.event.Event;

@Name("Kick Group Member")
@Description({"踢出群成员", "Kick group members out of group chat."})
@Examples({
        "on bot receive group message:",
        "\t # 脏话 = fuck",
        "\tbot-message's content is \"脏话\"",
        "\tkick group member by reason of \"你的话太多了\"",
        "kick group member"
})
@Since("1.0.0")
public class EffKickMember extends Effect {

    static {
        Skript.registerEffect(EffKickMember.class, "kick [group] %qqgroupmember% [(by reason of|because [of]|due to) %-string%]");
    }

    private Expression<Member> member;
    private Expression<String> kickMessage;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        member = (Expression<Member>) exprs[0];
        kickMessage = (Expression<String>) exprs[1];
        return true;
    }

    @Override
    protected void execute(Event e) {
        String reason = kickMessage != null ? kickMessage.getSingle(e) : "";
        Member single = member.getSingle(e);
        if (single instanceof NormalMember) ((NormalMember) single).kick(reason);
    }

    @Override
    public String toString(Event e, boolean debug) {
        return "kick group " + member.toString(e, debug) + (kickMessage != null ? " due to " + kickMessage.toString(e, debug) : "");
    }
}
