package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.message.data.Message;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Bot Send Message")
@Description({"机器人发送消息给好友/群/或者临时会话群成员", "Send messages to group chats, private chats or temporary chats."})
@Examples({
        "on bot receive group message:",
        "\tsend @group-member to qq-group",
        "\tsend \"Hello!\" to qq-group"
})
@Since("1.0.0")
public class EffSendMessage extends Effect {

    static {
        Skript.registerEffect(EffSendMessage.class,"send %qqmessage% to %qqcontact%");
    }

    private Expression<? extends Message> message;
    private Expression<? extends Contact> target;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        message = (Expression<? extends Message>) exprs[0];
        target = (Expression<? extends Contact>) exprs[1];
        return true;
    }

    @Override
    protected void execute(Event e) {
        target.getSingle(e).sendMessage(message.getSingle(e));
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "send " + message.toString(e, debug) + " to " + target.toString(e, debug);
    }
}
