package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.Mirai;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.MessageEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Bot Recall Message")
@Events("Bot Message Event")
@Description({"在接收消息事件中撤回一条消息", "Recall a message, is in Bot Message Event."})
@Examples({
        "on bot receive group message:",
        "\t # 脏话 = fuck",
        "\tbot-message's content is \"脏话\"",
        "\trecall message"
})
@Since("1.0.0")
public class EffRecallMessage extends Effect {

    static {
        Skript.registerEffect(EffRecallMessage.class, "[bot] [event] recall [(qq|bot)( |-)] message");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected void execute(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof MessageEvent) {
                Mirai.getInstance().recallMessage(event.getBot(), ((MessageEvent) event).getSource());
            }
        }
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "recall message";
    }
}
