package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.utils.ExternalResource;
import org.bukkit.event.Event;
import org.bukkit.scheduler.BukkitRunnable;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.skript.McQQBotSkriptPlugin;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

@Name("Bot Send URL Image Message")
@Description({"机器人发送图片给好友/群/或者临时会话群成员", "Send image to group chats, private chats or temporary chats."})
@Examples({
        "on bot receive group message:",
        "# 发送图片会比较慢 暂时无法与其他信息拼接 后续再考虑其他方式",
        "\tsend image url \"https://api.ixiaowai.cn/api/api.php\" to qq-group"
})
@Since("1.0.5")
public class EffUrlImageMessage extends Effect {

    static {
        Skript.registerEffect(EffUrlImageMessage.class,"send image url %string% to %qqcontact%");
    }

    private Expression<? extends String> url;
    private Expression<? extends Contact> target;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        url = (Expression<? extends String>) exprs[0];
        target = (Expression<? extends Contact>) exprs[1];
        return true;
    }

    @Override
    protected void execute(Event e) {
        new BukkitRunnable() {
            @Override
            public void run() {
                try {
                    URL url = new URL(EffUrlImageMessage.this.url.getSingle(e));
                    URLConnection urlConnection = url.openConnection();
                    ExternalResource.sendAsImage(urlConnection.getInputStream(), target.getSingle(e));
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }.runTaskAsynchronously(McQQBotSkriptPlugin.getPlugin(McQQBotSkriptPlugin.class));
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "send image url " + url.toString(e, debug) + " to " + target.toString(e, debug);
    }
}
