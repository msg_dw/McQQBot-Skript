package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.skript.util.Timespan;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.contact.NormalMember;
import org.bukkit.event.Event;

@Name("Mute Group Member")
@Description({"禁言/解除禁言群成员", "Mute or unmute a member in the group."})
@Examples({
        "on bot receive group message:",
        "\t # 脏话 = fuck",
        "\tbot-message's content is \"脏话\"",
        "\tmute group member for 2 days",
        "unmute group member"
})
@Since("1.0.0")
public class EffMuteMember extends Effect {

    static {
        Skript.registerEffect(EffMuteMember.class, "mute [group] %qqgroupmember% for %timespan%", "unmute [group] %qqgroupmember%");
    }

    private boolean isUn;
    private Expression<Member> member;
    private Expression<Timespan> time;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        isUn = matchedPattern == 1;

        member = (Expression<Member>) exprs[0];
        time = (Expression<Timespan>) exprs[1];

        return true;
    }

    @Override
    protected void execute(Event e) {
        Member single = member.getSingle(e);
        if (isUn) {
            if (single instanceof NormalMember) {
                ((NormalMember) single).unmute();
            }
        }else {
            single.mute((int) (time.getSingle(e).getMilliSeconds() / 1000));
        }
    }

    @Override
    public String toString(Event e, boolean debug) {
        return isUn ? "unmute " + member.toString(e, debug) : "mute " + member.toString(e, debug) + " for " + time.toString(e, debug);
    }
}
