package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.Description;
import ch.njol.skript.doc.Examples;
import ch.njol.skript.doc.Name;
import ch.njol.skript.doc.Since;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.contact.Group;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;

@Name("Mute Group All Member")
@Description({"禁言/解除禁言群所有成员", "Mute or unmute all in group chat"})
@Examples({
        "on bot receive group message:",
        "\tall members of mute qq-group",
        "\tall members of unmute qq-group"
})
@Since("1.0.0")
public class EffMuteAllMember extends Effect {

    static {
        Skript.registerEffect(EffMuteAllMember.class, "[all] member[s] of [(1¦un)]mute %qqgroup%");
    }

    private boolean isUn;
    private Expression<Group> group;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        group = (Expression<Group>) exprs[0];
        isUn = parseResult.mark == 1;
        return true;
    }

    @Override
    protected void execute(Event e) {
        group.getSingle(e).getSettings().setMuteAll(!isUn);
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "all members of " + (isUn ? "un" : "") + "mute " + group.toString(e, debug);
    }
}
