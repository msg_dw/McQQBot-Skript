package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.MemberJoinRequestEvent;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Accept Join Group")
@Events("Member Request Join Group")
@Description({"在请求加群事件中同意加群", "Agree members to join the group chat. is in Member Request Join Group Event."})
@Examples({
        "member request join group:",
        "\taccept join group"
})
@Since("1.0.0")
public class EffAcceptJoinGroup extends Effect {

    static {
        Skript.registerEffect(EffAcceptJoinGroup.class, "(allow|accept) join group");
    }

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        return true;
    }

    @Override
    protected void execute(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof MemberJoinRequestEvent) {
                ((MemberJoinRequestEvent) event).accept();
            }
        }
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "accept join group";
    }
}
