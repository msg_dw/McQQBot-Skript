package pro.sandiao.mcqqbot.skript.effects;

import ch.njol.skript.Skript;
import ch.njol.skript.doc.*;
import ch.njol.skript.lang.Effect;
import ch.njol.skript.lang.Expression;
import ch.njol.skript.lang.SkriptParser;
import ch.njol.util.Kleenean;
import net.mamoe.mirai.event.events.BotEvent;
import net.mamoe.mirai.event.events.MessageEvent;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.QuoteReply;
import org.bukkit.event.Event;
import org.jetbrains.annotations.Nullable;
import pro.sandiao.mcqqbot.event.BukkitMcBotEvent;

@Name("Bot Reply Message")
@Events("Bot Message Event")
@Description({"在接收消息事件中回复消息", "Reply message, is in Bot Message Event."})
@Examples({
        "# 回复信息 Test ",
        "on bot receive message:",
        "\treply \"Test\"",
        "# 引用回复 @Ta",
        "on bot receive group message:",
        "\tquote reply @group-member",
})
@Since("1.0.0")
public class EffReplyMessage extends Effect {

    static {
        Skript.registerEffect(EffReplyMessage.class, "[bot] [event] [(1¦quote)] reply %qqmessage%", "[bot] [event] [(1¦quote)] reply %string%");
    }

    private boolean isQuote;
    private Expression<Message> message;

    @Override
    public boolean init(Expression<?>[] exprs, int matchedPattern, Kleenean isDelayed, SkriptParser.ParseResult parseResult) {
        isQuote = parseResult.mark == 1;
        message = (Expression<Message>) exprs[0];
        return true;
    }

    @Override
    protected void execute(Event e) {
        if (e instanceof BukkitMcBotEvent) {
            BotEvent event = ((BukkitMcBotEvent) e).getEvent();
            if (event instanceof MessageEvent) {
                Message single = message.getSingle(e);
                if (isQuote) single = new QuoteReply(((MessageEvent) event).getSource()).plus(single);
                ((MessageEvent) event).getSubject().sendMessage(single);
            }
        }
    }

    @Override
    public String toString(@Nullable Event e, boolean debug) {
        return "bot" + (isQuote ? " quote " : " ") + "reply " + message.toString(e, debug);
    }
}
