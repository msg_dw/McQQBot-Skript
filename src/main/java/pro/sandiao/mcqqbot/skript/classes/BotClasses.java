package pro.sandiao.mcqqbot.skript.classes;

import ch.njol.skript.classes.ClassInfo;
import ch.njol.skript.classes.Parser;
import ch.njol.skript.classes.Serializer;
import ch.njol.skript.lang.ParseContext;
import ch.njol.skript.lang.VariableString;
import ch.njol.skript.registrations.Classes;
import ch.njol.yggdrasil.Fields;
import net.mamoe.mirai.Bot;
import net.mamoe.mirai.contact.Contact;
import net.mamoe.mirai.contact.Friend;
import net.mamoe.mirai.contact.Group;
import net.mamoe.mirai.contact.Member;
import net.mamoe.mirai.message.data.Message;
import net.mamoe.mirai.message.data.MessageChain;
import net.mamoe.mirai.message.data.MessageChainBuilder;
import net.mamoe.mirai.message.data.PlainText;

import java.io.NotSerializableException;
import java.io.StreamCorruptedException;

/**
 * 类型统一注册
 */
public class BotClasses {

    static {
        Classes.registerClass(new ClassInfo<>(Bot.class, "qqbot")
                .user("bots?")
                .name("QQ Bot")
                .description("机器人 可以用于机器人的其它操作", "Tencent QQ Robot, Used to operate the robot.")
                .examples("bot of \"Main\"")
                .since("1.0.0")
                .parser(new Parser<Bot>() {
                    @Override
                    public String toString(Bot o, int flags) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public String toVariableNameString(Bot o) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public boolean canParse(ParseContext context) {
                        return false;
                    }

                    @Override
                    public String getVariableNamePattern() {
                        return "\\d+";
                    }
                })
        );
        Classes.registerClass(new ClassInfo<>(Group.class, "qqgroup")
                .user("qq( |-)groups?")
                .before("qqcontact")
                .name("QQ Group")
                .description("QQ群", "QQ Group.")
                .examples("group 10086 of \"Main\" bot")
                .since("1.0.0")
                .parser(new Parser<Group>() {
                    @Override
                    public String toString(Group o, int flags) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public String toVariableNameString(Group o) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public boolean canParse(ParseContext context) {
                        return false;
                    }

                    @Override
                    public String getVariableNamePattern() {
                        return "\\d+";
                    }
                })
        );
        Classes.registerClass(new ClassInfo<>(Friend.class, "qqfriend")
                .user("qq( |-)friends?")
                .before("qqcontact")
                .name("QQ Friend")
                .description("QQ好友", "QQ Friend.")
                .examples("friend 10086 of \"Main\" bot")
                .since("1.0.0")
                .parser(new Parser<Friend>() {
                    @Override
                    public String toString(Friend o, int flags) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public String toVariableNameString(Friend o) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public boolean canParse(ParseContext context) {
                        return false;
                    }

                    @Override
                    public String getVariableNamePattern() {
                        return "\\d+";
                    }
                })
        );
        Classes.registerClass(new ClassInfo<>(Member.class, "qqgroupmember")
                .user("group( |-)members?")
                .before("qqcontact")
                .name("QQ Group Member")
                .description("QQ群内的成员", "Members in QQ Group.")
                .examples("member 10086 of %group%")
                .since("1.0.0")
                .parser(new Parser<Member>() {
                    @Override
                    public String toString(Member o, int flags) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public String toVariableNameString(Member o) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public boolean canParse(ParseContext context) {
                        return false;
                    }

                    @Override
                    public String getVariableNamePattern() {
                        return "\\d+";
                    }
                })
        );
        Classes.registerClass(new ClassInfo<>(Contact.class, "qqcontact")
                .user("qq( |-)contacts?")
                .after("qqgroup", "qqfriend", "qqgroupmember")
                .name("QQ Contact")
                .description("QQ联系人 代表群, 好友, 成员等")
                .description("send \"Test\" to %contact%")
                .since("1.0.1")
                .parser(new Parser<Contact>() {
                    @Override
                    public String toString(Contact o, int flags) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public String toVariableNameString(Contact o) {
                        return String.valueOf(o.getId());
                    }

                    @Override
                    public boolean canParse(ParseContext context) {
                        return false;
                    }

                    @Override
                    public String getVariableNamePattern() {
                        return "\\d+";
                    }
                })
        );
        Classes.registerClass(new ClassInfo<>(Message.class, "qqmessage")
                .user("qq( |-)messages?")
                .name("QQ Message")
                .description("QQ消息", "Message of ready to be sent or received.")
                .examples("qq-message of \"Text\"")
                .since("1.0.0")
                .parser(new Parser<Message>() {
                    @Override
                    public Message parse(String s, ParseContext context) {
                        if (VariableString.isQuotedCorrectly(s, true)) {
                            return new PlainText(VariableString.unquote(s, true));
                        }
                        return null;
                    }

                    @Override
                    public boolean canParse(ParseContext context) {
                        if (context == ParseContext.DEFAULT) return false;
                        return true;
                    }

                    @Override
                    public String toString(Message o, int flags) {
                        return o.contentToString();
                    }

                    @Override
                    public String toVariableNameString(Message o) {
                        return o.contentToString();
                    }

                    @Override
                    public String getVariableNamePattern() {
                        return ".*";
                    }
                }).serializer(new Serializer<Message>() {
                    @Override
                    public Fields serialize(Message o) throws NotSerializableException {
                        MessageChain messageChain;
                        if (o instanceof MessageChain) {
                            messageChain = (MessageChain) o;
                        } else {
                            MessageChainBuilder builder = new MessageChainBuilder();
                            messageChain = builder.append(o).build();
                        }
                        Fields f = new Fields();
                        f.putObject("json", MessageChain.serializeToJsonString(messageChain));
                        return f;
                    }

                    @Override
                    public void deserialize(Message o, Fields f) throws StreamCorruptedException, NotSerializableException {
                        assert false;;
                    }

                    @Override
                    protected Message deserialize(Fields fields) throws StreamCorruptedException, NotSerializableException {
                        String json = fields.getObject("json", String.class);
                        if (json == null)
                            throw new StreamCorruptedException();
                        return MessageChain.deserializeFromJsonString(json);
                    }

                    @Override
                    public boolean mustSyncDeserialization() {
                        return false;
                    }

                    @Override
                    protected boolean canBeInstantiated() {
                        return false;
                    }
                })
        );
    }

}
